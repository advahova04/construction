<!DOCTYPE html>
<html>
<head>
  <title>Construction</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <link rel="stylesheet" href="/css/app.css">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet"> 
</head>
<body>
<div class="hero">
  <div class="container">
    <div class="images-svg">
        <img src="" alt="">
    </div>
    <div class="row hero-block">
      <div class="col-12" >
        <div class="overlay">
          <div class="row">
            <div class="col-12 text-center">
              <h1>РЕАЛИЗУЕМ КРУПНЕЙШИЕ <br>ПРОЕКТЫ В РОССИИ</h1>
              <h2>стадионы, газопроводы, мосты, дамбы</h2>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="years-block">
  <div class="container">
    <div class="row">
      <div class="col-12 text-center">
        <div class="row">
          <div class="col-lg-3">
            <h1>26</h1>
            <h4>ЛЕТ</h4>
            <p>на рынке</p>
          </div>
          <div class="col-lg-3">
            <h1>26</h1>
            <h4>ЛЕТ</h4>
            <p>на рынке</p>
          </div>
          <div class="col-lg-3">
            <h1>26</h1>
            <h4>ЛЕТ</h4>
            <p>на рынке</p>
          </div>
          <div class="col-lg-3">
            <h1>26</h1>
            <h4>ЛЕТ</h4>
            <p>на рынке</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="our-project">
  <div class="container">
    <div class="overlay">
      <div class="row">
        <div class="col-12 text-center">
          <h3>НАШИ САМЫЕ БОЛЬШИЕ ПРОЕКТЫ</h3>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-4">
          <img src="../css/images/Rectangle2.png" alt="Изображение">
          <div class="divider"></div>
          <h4>Газпром Арена</h4>
          <p>Мы сделали самую красивую арену в Европе. Это открытие стало для нас прорывной точкой для развития на следующие десятилетия. Мы очень рады данному событию.</p>
        </div>
        <div class="col-lg-4">
          <img src="../css/images/Rectangle2.png" alt="Изображение">
          <div class="divider"></div>
          <h4>Газпром Арена</h4>
          <p>Мы сделали самую красивую арену в Европе. Это открытие стало для нас прорывной точкой для развития на следующие десятилетия. Мы очень рады данному событию.</p>
        </div>
        <div class="col-lg-4">
          <img src="../css/images/Rectangle2.png" alt="Изображение">
          <div class="divider"></div>
          <h4>Газпром Арена</h4>
          <p>Мы сделали самую красивую арену в Европе. Это открытие стало для нас прорывной точкой для развития на следующие десятилетия. Мы очень рады данному событию.</p>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="red">
  <div class="container">
    <div class="row red-block">
      <div class="col-lg-8">
        <div class="red-block-content">
          <h4>САМЫЕ УМНЫЕ ПРОЕКТЫ</h4>
          <h5>РЕАЛИЗУЕМ САМЫЕ СМЕЛЫЕ РЕШЕНИЯ В РОССИИ!</h5>
        </div>
      </div>
      <div class="col-lg-8">
        <div class="red-block-button">
          <img src="../css/images/text.svg" alt="Изображение" class="red-block-image">
          <p>ВАШ ЗАПРОС</p>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="what-we-do">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h3>ЧЕМ МЫ ЗАНИМАЕМСЯ?</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="dom">
                    <img src="../css/images/dom.svg" alt="Image 1">
                    <p>СТРОИТЕЛЬСТВО<br>ОФИСНЫХ ЗДАНИЙ</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="dom">
                    <img src="../css/images/dom.svg" alt="Image 2">
                    <p>СТРОИТЕЛЬСТВО<br>ОФИСНЫХ ЗДАНИЙ</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="dom">
                    <img src="../css/images/dom.svg" alt="Image 3">
                    <p>СТРОИТЕЛЬСТВО<br>ОФИСНЫХ ЗДАНИЙ</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="dom">
                    <img src="../css/images/dom.svg" alt="Image 4">
                    <p>СТРОИТЕЛЬСТВО<br>ОФИСНЫХ ЗДАНИЙ</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="dom">
                    <img src="../css/images/dom.svg" alt="Image 5">
                    <p>СТРОИТЕЛЬСТВО<br>ОФИСНЫХ ЗДАНИЙ</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="dom">
                    <img src="../css/images/dom.svg" alt="Image 6">
                    <p>СТРОИТЕЛЬСТВО<br>ОФИСНЫХ ЗДАНИЙ</p>
                </div>
            </div>
        </div>
    </div>
</div>

</div>
</body>
</html>
